import React, { useEffect, useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { connect } from "./redux/blockchain/blockchainActions";
import { fetchData } from "./redux/data/dataActions";
import * as s from "./styles/globalStyles";
import styled from "styled-components";

const truncate = (input, len) =>
  input.length > len ? `${input.substring(0, len)}...` : input;

export const PunkKnightButtons = styled.button`
  padding: 10px;
  font-size: 100%;
  font-family: coder;
  border-radius: 10px;
  border: solid;
  border-color: white;
  background-color: green;
  padding: 10px;
  font-weight: bold;
  color: var(--secondary-text);
  width: 200px;
  height: 60px;
  cursor: pointer;
  :active {
    box-shadow: none;
    -webkit-box-shadow: none;
    -moz-box-shadow: none;
  }
`;

export const AmountButtons = styled.button`
  padding: 10px;
  border-radius: 20%;
  border: none;
  background-color: lichtgrey;
  padding: 10px;
  font-weight: bold;
  font-size: 25px;
  color: black;
  width: 100px;
  height: 50px;
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: center;
  :active {
    box-shadow: none;
    -webkit-box-shadow: none;
    -moz-box-shadow: none;
  }
`;

export const PunkKnightWrapper = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  justify-content: stretched;
  align-items: center;
  width: justify;
  @media (min-width: 767px) {
    flex-direction: row;
  }
`;

export const PunkKnightLogo = styled.img`
  width: 200px;
  @media (min-width: 767px) {
    width: 500px;
  }
  transition: width 0.5s;
  transition: height 0.5s;
`;

export const PunkKnightImg = styled.img`
  box-shadow: 0px 5px 11px 2px rgba(0, 0, 0, 0.7);
  border: 5px solid var(--secondary);
  background-color: var(--accent);
  border-radius: 10%;
  width: 200px;
  @media (min-width: 900px) {
    width: 250px;
  }
  @media (min-width: 1000px) {
    width: 200px;
  }
  transition: width 0.5s;
`;

export const StyledLink = styled.a`
  color: var(--secondary);
  text-decoration: none;
`;

function App() {
  const dispatch = useDispatch();
  const blockchain = useSelector((state) => state.blockchain);
  const data = useSelector((state) => state.data);
  const [mintingNfk, setMintingNfk] = useState(false);
  const [feedback, setFeedback] = useState(
    `Click mint knight(s) to mint your NFK(s).`
  );
  const [mintAmount, setMintAmount] = useState(1);
  const [CONFIG, SET_CONFIG] = useState({
    CONTRACT_ADDRESS: "",
    BLOCK_EXPLORER: "",
    NETWORK: {
      NAME: "",
      SYMBOL: "",
      ID: 0,
    },
    NFK_NAME: "",
    SYMBOL: "",
    MAX_KNIGHTS: 1,
    BNB_AMOUNT: 0,
    NFK_PRICE: 0,
    GAS_LIMIT: 0,
    MARKETPLACE: "",
    MARKETPLACE_LINK: "",
    SHOW_BACKGROUND: false,
  });

  const mintNFKs = () => {
    let publicPrice = CONFIG.BNB_AMOUNT;
    let gasLimit = CONFIG.GAS_LIMIT;
    let bnbAmount = String(publicPrice * mintAmount);
    let totalGasLimit = String(gasLimit * mintAmount);
    console.log("publicPrice: ", bnbAmount);
    console.log("Gas limit: ", totalGasLimit);
    setFeedback(`Minting your Punk Knight(s)...`);
    setMintingNfk(true);
    blockchain.smartContract.methods
      .mintKnight(mintAmount)
      .send({
        gasLimit: String(totalGasLimit),
        to: CONFIG.CONTRACT_ADDRESS,
        from: blockchain.account,
        value: bnbAmount,
      })
      .once("error", (err) => {
        console.log(err);
        setFeedback("Sorry, something went wrong please try again later.");
        setMintingNfk(false);
      })
      .then((receipt) => {
        console.log(receipt);
        setFeedback(`You succesfully minted your knight(s)`);
        setMintingNfk(false);
        dispatch(fetchData(blockchain.account));
      });
  };

  const decrementMintAmount = () => {
    let newMintAmount = mintAmount - 1;
    if (newMintAmount < 1) {
      newMintAmount = 1;
    }
    setMintAmount(newMintAmount);
  };

  const incrementMintAmount = () => {
    let newMintAmount = mintAmount + 1;
    if (newMintAmount > 5) {
      newMintAmount = 5;
    }
    setMintAmount(newMintAmount);
  };

  const getData = () => {
    if (blockchain.account !== "" && blockchain.smartContract !== null) {
      dispatch(fetchData(blockchain.account));
    }
  };

  const getConfig = async () => {
    const configResponse = await fetch("/config/config.json", {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    });
    const config = await configResponse.json();
    SET_CONFIG(config);
  };

  useEffect(() => {
    getConfig();
  }, []);

  useEffect(() => {
    getData();
  }, [blockchain.account]);

  return (
    <s.Screen>
      <s.PkContainer
        flex={1}
        ai={"center"}
        style={{ padding: 24, backgroundColor: "var(--primary)" }}
        image={CONFIG.SHOW_BACKGROUND ? "/config/images/bg.gif" : null}
      >
        <a href="https://punkknights.com">
          <PunkKnightLogo alt={"logo"} src={"/config/images/logo.gif"} />
        </a>

        <PunkKnightWrapper flex={1} style={{ padding: 24 }} test>
          <s.PkLargeSpacer />
          <s.PkContainer
            flex={2}
            jc={"center"}
            ai={"center"}
            style={{
              backgroundColor: "var(--accent)",
              padding: 24,
              borderRadius: 24,
              border: "4px solid var(--secondary)",
              boxShadow: "0px 5px 11px 2px rgba(0,0,0,0.7)",
            }}
          >
            <s.MintStatusTitle
              style={{
                textAlign: "center",
                fontWeight: "bold",
                color: "var(--accent-text)",
              }}
            >
              punk knights minting
            </s.MintStatusTitle>
            <s.StatusDescription
              style={{ textAlign: "center", color: "var(--accent-text)" }}
            >
              Minting is now open for the public with a maximum mint amount of 5 knights
            </s.StatusDescription>
            <s.PkSmallSpacer />
            <PunkKnightImg alt={"example"} src={"/config/images/example.gif"} />
            <s.KnightSupply
              style={{
                textAlign: "center",
                fontWeight: "bold",
                color: "var(--accent-text)",
              }}
            >
              {data.totalSupply} / {CONFIG.MAX_KNIGHTS}
            </s.KnightSupply>
            <s.PkSmallSpacer />
            {Number(data.totalSupply) >= CONFIG.MAX_KNIGHTS ? (
              <>
                <s.TextTitle
                  style={{ textAlign: "center", color: "var(--accent-text)" }}
                >
                  The sale has ended.
                </s.TextTitle>
                <s.TextDescription
                  style={{ textAlign: "center", color: "var(--accent-text)" }}
                >
                  You can still find {CONFIG.NFK_NAME} on
                </s.TextDescription>
                <s.PkSmallSpacer />
                <StyledLink target={"_blank"} href={CONFIG.MARKETPLACE_LINK}>
                  {CONFIG.MARKETPLACE}
                </StyledLink>
              </>
            ) : (
              <>
                <s.TextTitle
                  style={{ textAlign: "center", color: "var(--accent-text)" }}
                >
                  {CONFIG.NFK_PRICE} {CONFIG.NETWORK.SYMBOL} per {CONFIG.SYMBOL}{" "}
                </s.TextTitle>
                <s.PkSmallSpacer />

                {blockchain.account === "" ||
                blockchain.smartContract === null ? (
                  <s.PkContainer ai={"center"} jc={"center"}>
                    <s.TextDescription
                      style={{
                        textAlign: "center",
                        color: "var(--accent-text)",
                      }}
                    >
                      Connect Metamask to the {CONFIG.NETWORK.NAME}
                    </s.TextDescription>
                    <s.PkSmallSpacer />
                    <PunkKnightButtons
                      onClick={(e) => {
                        e.preventDefault();
                        dispatch(connect());
                        getData();
                      }}
                    >
                      CONNECT WALLET
                    </PunkKnightButtons>
                    {blockchain.errorMsg !== "" ? (
                      <>
                        <s.PkSmallSpacer />
                        <s.TextDescription
                          style={{
                            textAlign: "center",
                            color: "var(--accent-text)",
                          }}
                        >
                          {blockchain.errorMsg}
                        </s.TextDescription>
                      </>
                    ) : null}
                  </s.PkContainer>
                ) : (
                  <>
                    <s.TextDescription
                      style={{
                        textAlign: "center",
                        color: "var(--accent-text)",
                      }}
                    >
                      {feedback}
                    </s.TextDescription>
                    <s.PkMediumSpacer />
                    <s.PkContainer ai={"center"} jc={"center"} fd={"row"}>
                      <AmountButtons
                        style={{ lineHeight: 0.4 }}
                        disabled={mintingNfk ? 1 : 0}
                        onClick={(e) => {
                          e.preventDefault();
                          decrementMintAmount();
                        }}
                      >
                        -
                      </AmountButtons>
                      <s.PkMediumSpacer />
                      <s.TextDescription
                        style={{
                          textAlign: "center",
                          fontSize: "250%",
                          color: "var(--accent-text)",
                        }}
                      >
                        {mintAmount}
                      </s.TextDescription>
                      <s.PkMediumSpacer />
                      <AmountButtons
                        disabled={mintingNfk ? 1 : 0}
                        onClick={(e) => {
                          e.preventDefault();
                          incrementMintAmount();
                        }}
                      >
                        +
                      </AmountButtons>
                    </s.PkContainer>
                    <s.PkSmallSpacer />
                    <s.PkContainer ai={"center"} jc={"center"} fd={"row"}>
                      <PunkKnightButtons
                        disabled={mintingNfk ? 1 : 0}
                        onClick={(e) => {
                          e.preventDefault();
                          mintNFKs();
                          getData();
                        }}
                      >
                        {mintingNfk ? "MINTING KNIGHT(S)" : "MINT KNIGHT(S)"}
                      </PunkKnightButtons>
                    </s.PkContainer>
                  </>
                )}
              </>
            )}
          </s.PkContainer>
          <s.PkLargeSpacer />
        </PunkKnightWrapper>
        <s.PkMediumSpacer />
        <s.PkContainer jc={"center"} ai={"center"} style={{ width: "70%" }}>
          <s.ContractAddress
            style={{
              textAlign: "center",
              color: "var(--primary-text)",
            }}
          >
            CONTRACT ADDRESS:{" "}
            <StyledLink target={"_blank"} href={CONFIG.BLOCK_EXPLORER}>
              {truncate(CONFIG.CONTRACT_ADDRESS, 50)}
            </StyledLink>
          </s.ContractAddress>
          <s.FooterText
            style={{
              textAlign: "center",
              color: "var(--primary-text)",
            }}
          >
            Copyright © 2022 Punk Knights | Powered by FireChain
          </s.FooterText>
        </s.PkContainer>
      </s.PkContainer>
    </s.Screen>
  );
}

export default App;
